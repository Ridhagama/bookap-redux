import AsyncStorage from '@react-native-async-storage/async-storage';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import ReduxLogger from 'redux-logger';
import {persistReducer, persistStore} from 'redux-persist';
import reduxthunk from 'redux-thunk';
import {HomeReducer, LoginReducer, RegisterReducer, DetailReducer} from '../reducer';

const persistConfig = {
  whitelist: ['Login'],
  key: 'root',
  storage: AsyncStorage,
};

const RootReducer = {
  Login: LoginReducer,
  Register: RegisterReducer,
  Home: HomeReducer,
  DetailBook: DetailReducer,
};

const configpersist = persistReducer(
  persistConfig,
  combineReducers(RootReducer),
);

export const Store = createStore(
  configpersist,
  applyMiddleware(reduxthunk, ReduxLogger),
);
export const PersistStore = persistStore(Store);
