import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {registerAction} from '../../redux/action';
import {back} from '../../assets'

const Register = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [fullname, setName] = useState('');
  const dispatch = useDispatch();
  const onSubmit = () => {
    dispatch(registerAction(fullname, email, password, navigation));
  };
  return (
    <View testID='RegisterView'>
      <View style={styles.container}>
        <TextInput
          onChangeText={text => setName(text)}
          value={fullname}
          style={styles.name}
          placeholder="FULL NAME"
          placeholderTextColor={'black'}
          testID='fullnameInput'
          >
          </TextInput>
      </View>
      <View style={styles.container2}>
        <TextInput
          onChangeText={text => setEmail(text)}
          value={email}
          style={styles.email}
          placeholder="EMAIL"
          placeholderTextColor={'black'}
          testID='emailInput'
          >
          </TextInput>
      </View>
      <View style={styles.container3}>
        <TextInput
          onChangeText={text => setPassword(text)}
          value={password}
          style={styles.password}
          placeholder="PASSWORD"
          placeholderTextColor={'black'}
          testID='passwordInput'
          >
          </TextInput>
      </View>
      <View style={styles.Registerstyle}>
        <TouchableOpacity testID='RegisterButton' onPress={() => onSubmit()}>
          <View style={styles.RegisterButton}>
            <Text>Register</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.Already}>
        <Text>Already have and account?</Text>
      </View>
      <TouchableOpacity testID='LoginButton' onPress={() => navigation.navigate('Login')}>
        <View style={styles.Login}>
          <Text>Login</Text>
        </View>
      </TouchableOpacity>
      <View>
        {/* <Image source={back} style={styles.Back}></Image> */}
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  name: {
    width: 250,
    height: 50,
    borderWidth: 2,
    borderRadius: 6,
    marginTop: 10,
  },
  container: {
    alignItems: 'center',
    marginTop: 20,
  },
  email: {
    width: 250,
    height: 50,
    borderWidth: 2,
    borderRadius: 6,
    marginTop: 10,
  },
  container2: {
    alignItems: 'center',
    marginTop: 20,
  },
  container3: {
    alignItems: 'center',
    marginTop: 20,
  },
  password: {
    width: 250,
    height: 50,
    borderWidth: 2,
    borderRadius: 6,
    marginTop: 10,
  },
  Register: {
    alignItems: 'center',
    marginTop: 6,
  },
  Registerstyle: {
    alignItems: 'center',
    marginTop: 6,
  },
  RegisterButton: {
    backgroundColor: '#E83A14',
    width: 300,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
  },
  Already: {
    alignItems: 'center',
    marginTop: 6,
  },
  Login: {
    alignItems: 'center',
    marginTop: 6,
  },
  Back: {
    width: 50,
    height: 50,
  }
});
