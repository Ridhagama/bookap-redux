import {StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import React, {useEffect} from 'react';
import {ICbook} from '../../assets';
import { useSelector } from 'react-redux';


const Splash = ({navigation}) => {
  const GetToken = useSelector(state => state.Login.token)
  useEffect(() => {
    setTimeout(() => {
      if (GetToken) {
        navigation.replace('Home');
      }
      else {
        navigation.replace('Login')
      }
      
    });
  }, [5000]);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="black" />
      <Image source={ICbook} style={styles.Logo} />
      <Text style={styles.MovieApp}> BookApp</Text>
      <Text style={styles.Name}>Gama</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#A97155'
  },
  Logo: {
    width: 100,
    height: 100,
  },
  MovieApp: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  Name: {
    fontSize: 14,
    position: 'absolute',
    bottom: 18,
  },
});
