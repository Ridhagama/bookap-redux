import React from 'react';
import {TouchableOpacity, View, Image} from 'react-native';
import {back} from '../../assets';

import Video from 'react-native-video';

const Videoplay = ({navigation}) => {
  return (
    <View>
      <View style={{ marginBottom: 15}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image source={back} style={{width: 30, height: 30}}></Image>
          <Video
            controls
            resizeMode="cover"
            source={{
              uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
            }}
            style={{
              width: '100%',
              height: 300,
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Videoplay;
