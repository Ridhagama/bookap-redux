import {
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect} from 'react';
import {back, heart, share} from '../../assets';
import {useDispatch, useSelector} from 'react-redux';
import {detailAction} from '../../redux/action';
import {CardDetail} from '../../components';

const DetailBook = ({route, navigation}) => {
  const {itemId} = route.params;
  const dispatch = useDispatch();
  console.log(itemId);
  const DataDetail = useSelector(state => state.DetailBook.DetailBooks);
  const GetToken = useSelector(state => state.Login.token);
  const loading = useSelector(state => state.DetailBook.loading);

  useEffect(() => {
    dispatch(detailAction(GetToken, itemId));
    console.log('Testing data :', DataDetail);
  }, []);

  return (
    <>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 12, marginHorizontal: 8,}}>
            <View style={styles.inBack}>
              <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <Image source={back} style={styles.Back}></Image>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image source={heart} style={styles.heart} ></Image>
              <Image source={share} style={styles.share}></Image>
            </View>
          </View>

          <View style={styles.container}>
            {/* <Image source={Duncan} style={styles.Duncan}></Image> */}
            <CardDetail
              source={DataDetail.cover_image}
              title={DataDetail.title}
              author={DataDetail.author}
            />
          </View>
        </View>
      )}
    </>
  );
};

export default DetailBook;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Back: {
    width: 30,
    height: 30,
  },
  heart: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  share: {
    width: 30,
    height: 30,
  },
  container: {
    flexDirection: 'row',
  },
  ImageDuncan: {},
  Duncan: {
    height: windowHeight * 0.3,
    width: windowWidth * 0.3,
    borderRadius: 6,
    marginRight: 6,
  },
  inBack: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
