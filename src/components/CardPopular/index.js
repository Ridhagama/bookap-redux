import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

const CardRecommended = ({
  source,
  title,
  author,
  publisher,
  rating,
  price,
  onPress,
}) => {
  return (
    <View style={styles.card}>
      <TouchableOpacity onPress={onPress}>
        <View>
          <Image source={source} style={styles.CardPopular} />
        </View>
        <View style={styles.description}>
          <Text
            style={{
              marginRight: 6,
              flexWrap: 'wrap',
              flex: 1,
              fontWeight: 'bold',
            }}>
            {title}
          </Text>
          <Text style={{fontWeight: 'bold'}}>{author}</Text>
          <Text style={{fontWeight: 'bold'}}>{publisher}</Text>
          <Text style={{fontWeight: 'bold'}}>{rating}</Text>
          <Text style={{fontWeight: 'bold'}}>{price}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CardRecommended;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  CardPopular: {
    height: windowHeight * 0.27,
    width: windowWidth * 0.3,
    borderRadius: 8,
    marginRight: 10,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  description: {
    marginRight: 10,
    flexDirection: 'column',
  },
  card: {
    width: windowWidth * 0.3,
    marginRight: 10,
  },
});
