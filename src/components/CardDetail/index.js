import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import React from 'react';

const CardDetail = ({source, title, author, publisher}) => {
  return (
    <View style={styles.card}>
        <Image source={{uri: source}} style={styles.CardPopular} />
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.author}>{author}</Text>
        <Text style={styles.publisher}>{publisher}</Text>
      </View>
    </View>
  );
};

export default CardDetail;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
card: {
  flexDirection: 'row',
  paddingHorizontal: windowWidth * 0.06,
  // paddingVertical: windowHeight * 0.27,
  marginTop: 24,
  borderRadius: 8,
  flex: 1,
},
titleWrapper: {
  marginLeft: 11,
  marginTop: 4,
  flex: 3,

},
title: {
  fontSize: 15,
  color: 'black',
  fontWeight: 'bold',
  marginBottom: 10,
},
CardPopular: {
  width: 600,
  height: 200,
  flex: 1
}
});
