import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

const CardRecommended = ({source, onPress}) => {
  return (
    <View>
      <TouchableOpacity onPress={onPress}>
        <View>
          <Image source={source} style={styles.CardRecommended} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CardRecommended;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  CardRecommended: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    borderRadius: 8,
    marginRight: 10,
  },
});
