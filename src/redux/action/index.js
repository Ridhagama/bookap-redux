import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_LOADING,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  REGISTER_LOADING,
  HOME_SUCCESS,
  HOME_FAILED,
  HOME_LOADING,
  HOMEPOPULAR,
  DETAIL_SUCCESS,
  DETAIL_FAILED,
  DETAIL_LOADING,
  LOGOUT,
  REFRESH_SUCCESS,
  SET_ONLINE,
} from '../type';
import axios from 'axios';

export const login_success = data => ({
  type: LOGIN_SUCCESS,
  payload: data,
});

export const login_failed = data => ({
  type: LOGIN_FAILED,
  payload: data,
});

export const login_loading = data => ({
  type: LOGIN_LOADING,
  payload: data,
});

export const logout = () => ({
  type: LOGOUT,
});

export const registerSucces = data => ({
  type: REGISTER_SUCCESS,
  payload: data,
});
export const registerFailed = data => ({
  type: REGISTER_FAILED,
  payload: data,
});
export const registerLoading = data => ({
  type: REGISTER_LOADING,
  payload: data,
});

export const homeSucces = data => ({
  type: HOME_SUCCESS,
  payload: data,
});
export const homeFailed = data => ({
  type: HOME_FAILED,
  payload: data,
});
export const homeLoading = data => ({
  type: HOME_LOADING,
  payload: data,
});
export const homepopular = data => ({
  type: HOMEPOPULAR,
  payload: data,
});
export const refreshHome = data => ({
  type: REFRESH_SUCCESS,
  payload: data,
});
export const setOnline = data => ({
  type: SET_ONLINE,
  payload: data,
})

export const detailSuccess = data => ({
  type: DETAIL_SUCCESS,
  payload: data,
});
export const detailFailed = data => ({
  type: DETAIL_FAILED,
  payload: data,
});
export const detailLoading = data => ({
  type: DETAIL_LOADING,
  payload: data,
});

export const login_action = (email, password, navigation) => async dispatch => {
  dispatch({type: LOGIN_LOADING});
  try {
    await axios
      .post('http://code.aldipee.com/api/v1/auth/login', {
        email: email,
        password: password,
      })
      .then(res => {
        if (res.data.tokens.access.token) {
          navigation.replace('Home');
          dispatch(login_success(res.data.tokens.access.token));
          alert('Sukses');
        }
      });
  } catch (error) {
    dispatch(login_failed(error.message));
    console.log(error.message);
    alert('Gagal');
  }
};

export const registerAction =
  (fullname, email, password, navigation) => async dispatch => {
    console.log('data', fullname, email, password);
    try {
      await axios
        .post('http://code.aldipee.com/api/v1/auth/register', {
          email: email,
          password: password,
          name: fullname,
        })
        .then(() => {
          dispatch(registerSucces());
          navigation.navigate('Success');
          alert('register success');
        });
    } catch (error) {
      dispatch(registerFailed(error.message));
      alert('Register failed');
      console.log(error);
    }
  };

export const getAction = (token, limit) => async dispatch => {
  dispatch(homeLoading(true));
  console.log(token);
  try {
    await axios
      .get('http://code.aldipee.com/api/v1/books', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {limit},
      })
      .then(res => {
        dispatch(refreshHome(false));
        dispatch(homeSucces(res.data.results));
      });
  } catch (error) {
    dispatch(refreshHome(false));
    dispatch(homeLoading(false));
    console.log(error.message);
  }
};

export const getPopular = token => async dispatch => {
  dispatch(homeLoading(true));
  console.log(token);
  try {
    await axios
      .get('http://code.aldipee.com/api/v1/books', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(res => {
        dispatch(homepopular(res.data.results));
      });
  } catch (error) {
    dispatch(refreshHome(false));
    dispatch(homeLoading(false));
    console.log(error.message);
  }
};

export const detailAction = (token, id) => async dispatch => {
  dispatch(detailLoading(true));
  console.log(token);
  try {
    await axios
      .get(`http://code.aldipee.com/api/v1/books/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(res => {
        dispatch(detailSuccess(res.data));
      });
  } catch (error) {
    dispatch(detailFailed(false));
    console.log(error.message);
  }
};


