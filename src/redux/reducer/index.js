import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_LOADING,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  REGISTER_LOADING,
  HOME_SUCCESS,
  HOME_FAILED,
  HOME_LOADING,
  LOGOUT,
  DETAIL_SUCCESS,
  DETAIL_FAILED,
  DETAIL_LOADING,
  HOMEPOPULAR,
  REFRESH_SUCCESS,
  SET_ONLINE,
} from '../type';

const initialLoginState = {
  token: null,
  loading: false,
  errormessage: '',
};

const initialRegisterState = {
  token: null,
  loading: false,
  errormessage: '',
};

const initialGetData = {
  bookPopular: [],
  bookRecomended: [],
  loading: false,
  errormessage: '',
  Isrefresh: false,
  setOnline: true,
};

const initialDetailBookState = {
  DetailBooks: [],
  loading: false,
  errormessage: '',
  Isrefresh: false,
};

export const LoginReducer = (state = initialLoginState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload,
        loading: false,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        errormessage: action.payload,
        loading: false,
      };
    case LOGIN_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case LOGOUT:
      return {
        ...state,
        token: null,
      };
    default:
      return state;
  }
};

export const RegisterReducer = (state = initialRegisterState, action) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return {
        ...state,
        token: action.payload,
        loading: false,
      };
    case REGISTER_FAILED:
      return {
        ...state,
        errormessage: action.payload,
        loading: false,
      };
    case REGISTER_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export const HomeReducer = (state = initialGetData, action) => {
  switch (action.type) {
    case HOME_SUCCESS:
      return {
        ...state,
        bookRecomended: action.payload,
        loading: false,
      };
    case HOME_FAILED:
      return {
        ...state,
        errormessage: action.payload,
        loading: false,
      };
    case HOME_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case HOMEPOPULAR:
      return {
        ...state,
        bookPopular: action.payload,
        loading: false,
      };
      case REFRESH_SUCCESS:
        return {
          ...state,
          Isrefresh: action.payload,
        }
        case SET_ONLINE:
          return {
            ...state,
            setOnline: action.payload,
          }
    default:
      return state;
  }
};

export const DetailReducer = (state = initialDetailBookState, action) => {
  switch (action.type) {
    case DETAIL_SUCCESS:
      return {
        ...state,
        DetailBooks: action.payload,
        loading: false,
      };
    case DETAIL_FAILED:
      return {
        ...state,
        errormessage: action.payload,
        loading: false,
      };
    case DETAIL_LOADING:
      return {
        ...state,
        loading: true,
      };
      case REFRESH_SUCCESS:
        return {
          ...state,
          Isrefresh: action.payload,
        }
    default:
      return state;
  }
};
