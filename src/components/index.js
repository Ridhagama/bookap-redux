import CardRecommended from "./CardRecommended";
import CardPopular from "./CardPopular"
import CardDetail from "./CardDetail"
import CardDetail2 from "./CardDetail2";
import NoInternet from "./NoInternet";

export {CardRecommended, CardPopular, CardDetail, CardDetail2, NoInternet}