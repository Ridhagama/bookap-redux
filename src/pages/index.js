import DetailBook from './DetailBook'
import Home from './Home'
import Login from './Login'
import Register from './Register'
import Splash from './Splash'
import Success from './Success'
import Pdfku from './Pdf'
import Videoplay from './Videoplay'

export {DetailBook, Home, Login, Register, Splash, Success, Pdfku, Videoplay}
