import React from 'react';
import renderer from 'react-test-renderer';
import Home from '../src/Home';
import 'react-native'


test('Home Snapshot', () => {
  const snap = renderer
    .create(<Home />)
    .toJSON();
  expect(snap).toMatchSnapshot();
});