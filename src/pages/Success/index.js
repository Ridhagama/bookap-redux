import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {check} from '../../assets';

const Success = ({navigation}) => {
  return (
    <View testID='SuccessView'>
      <View style={styles.container}>
        <Text style={styles.completed}>Registration Completed!</Text>
      </View>
      <View style={styles.image}>
        <Image source={check} style={styles.check}></Image>
      </View>
      <View style={styles.sent}>
        <Text style={styles.we}>we sent email verification to </Text>
      </View>
      <View style={styles.your}>
        <Text style={styles.email}>your email</Text>
      </View>
      <View style={styles.backbutton}>
      <TouchableOpacity testID='successbutton'  onPress={() => navigation.navigate('Login')}>
        <View style={styles.backstyle}>
          <Text>back to login</Text>
        </View>
      </TouchableOpacity>
      </View>
    </View>
  );
};

export default Success;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 55,
  },
  completed: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  check: {
    width: 100,
    height: 100,
    marginTop: 30,
    borderRadius: 6,
  },
  image: {
    alignItems: 'center',
    marginTop: 100,
    marginBottom: 25,
  },
  sent: {
    alignItems: 'center',
    marginTop: 25,
  },
  we: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  your: {
    alignItems: 'center',
    marginTop: 6,
  },
  email: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  backbutton: {
    alignItems: 'center',
    marginTop: 6,
  },
  backstyle: {
    backgroundColor: '#0093AB',
    width: 400,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    marginTop: 140,
  }
});
