import React, {useEffect} from 'react';
import Router from './router';
import {NavigationContainer} from '@react-navigation/native';
import {Provider, useDispatch, useSelector} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {PersistStore, Store} from './redux/store';
import NetInfo from '@react-native-community/netinfo';
import {NoInternet} from './components';
import { setOnline } from './redux/action';

const Wrapper = () => {
  const online = useSelector(state => state.Home.setOnline);
  const dispatch = useDispatch();
  const removeNetInfo = () => {
    NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      dispatch(setOnline(!offline));
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  }, []);
  return (
    <NavigationContainer>
      {online ? <Router /> : <NoInternet />}
    </NavigationContainer>
  );
};
const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={PersistStore}>
        <Wrapper />
      </PersistGate>
    </Provider>
  );
};

export default App;
