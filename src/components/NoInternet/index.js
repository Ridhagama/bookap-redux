import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const NoInternet = () => {
  return (
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: "#A97155"}}>
      <Text style={{fontSize: 14, fontWeight: 'bold'}}>NoInternet</Text>
    </View>
  )
}

export default NoInternet

const styles = StyleSheet.create({})