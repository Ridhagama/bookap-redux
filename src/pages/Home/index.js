import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
  RefreshControl,
} from 'react-native';
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getAction, getPopular, logout, refreshHome} from '../../redux/action';
import {CardRecommended, CardPopular} from '../../components';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const Recommended1 = useSelector(state => state.Home.bookRecomended);
  const Popular = useSelector(state => state.Home.bookPopular);
  const GetToken = useSelector(state => state.Login.token);
  const loading = useSelector(state => state.Home.loading);
  const Refresh1 = useSelector(state => state.Home.Isrefresh);

  const RefreshOn = () => {
    dispatch(refreshHome(true));
    dispatch(getAction(token, 6));
    dispatch(getPopular(token));
  };

  const Logout = () => {
    Alert.alert('warning', 'anda ingin keluar?', [
      {
        text: 'tidak',
      },
      {
        text: 'ya',
        onPress: () => {
          dispatch(logout());
          navigation.replace('Splash');
        },
      },
    ]);
  };

  useEffect(() => {
    dispatch(getAction(GetToken, 6));
    console.log('Testing data :', Recommended1);
  }, []);

  useEffect(() => {
    dispatch(getPopular(GetToken));
    console.log('Testing data :', getPopular);
  }, []);

  useEffect(() => {
    dispatch(refreshHome(true));
  });

  return (
    <>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <View testID='Homeview' style={styles.container}>
          <ScrollView
            RefreshControl={
              <RefreshControl
                refreshing={Refresh1}
                OnRefresh={() => RefreshOn()}
              />
            }>
            <View>
              <View style={styles.name}>
                <Text style={styles.Good}>Good Morning, Gama!</Text>
                <TouchableOpacity onPress={Logout}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      backgroundColor: 'grey',
                      width: 70,
                      height: 20,
                      paddingLeft: 5,
                    }}>
                    LOGOUT
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{marginTop: 15, marginLeft: 10}}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Videoplay')}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      backgroundColor: 'grey',
                      width: 70,
                      height: 20,
                      paddingLeft: 5,
                    }}>
                    VIDEO
                  </Text>
                </TouchableOpacity>
                <View style={{marginTop: 15, marginLeft: 0}}>
                  <TouchableOpacity onPress={() => navigation.navigate('Pdfku')}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      backgroundColor: 'grey',
                      width: 70,
                      height: 20,
                      paddingLeft: 5,
                    }}>
                    PDF
                  </Text>
                  </TouchableOpacity>
                 
                </View>
              </View>
              <View style={styles.Recommended}>
                <Text style={styles.recom}>Recommended</Text>
              </View>
              <ScrollView testID='scrollhorizontal' horizontal showsHorizontalScrollIndicator={false}>
                <View style={styles.ImageDuncan}>
                  {Popular.map(item => {
                    return (
                      <CardRecommended
                        key={item.id}
                        onPress={() =>
                          navigation.navigate('Detail Book', {itemId: item.id})
                        }
                        source={{uri: item.cover_image}}
                      />
                    );
                  })}
                </View>
              </ScrollView>

              <View style={styles.Popular}>
                <Text style={styles.Book}>Popular Book</Text>
              </View>
              <ScrollView testID='scrollvertical' vertical>
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginTop: 8,
                  }}>
                  {Popular.map(item => {
                    return (
                      <CardPopular
                        key={item.id}
                        source={{uri: item.cover_image}}
                        onPress={() =>
                          navigation.navigate('Detail Book', {itemId: item.id})
                        }
                        title={item.title}
                        author={item.author}
                        publisher={item.publisher}
                        rating={item.average_rating}
                        price={`Rp. ${parseFloat(item.price).toLocaleString(
                          'id-ID',
                        )}`}
                      />
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          </ScrollView>
        </View>
      )}
    </>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#A97155',
  },
  name: {
    marginTop: 30,
    marginLeft: 10,
  },
  Good: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  Recommended: {
    marginTop: 28,
    marginLeft: 10,
  },
  recom: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  Duncan: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
  },
  Duncan1: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
  },
  Duncan2: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
  },
  Duncan3: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
  },
  ImageDuncan: {
    marginRight: 5,
    flexDirection: 'row',
    marginTop: 8,
  },
  Popular: {
    marginTop: 1,
    marginLeft: 10,
  },
  Book: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
  },
  Get: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  Get1: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  Get2: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  Get3: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  HP0: {
    flexDirection: 'row',
    marginTop: 6,
  },
  HP: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  HP1: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  HP2: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
  HP3: {
    height: windowHeight * 0.25,
    width: windowWidth * 0.3,
    marginLeft: 8,
    borderRadius: 6,
    marginTop: 6,
  },
});
