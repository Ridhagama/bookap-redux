import Pdf from 'react-native-pdf';
import React from 'react';
import {View, Text, Button, TouchableOpacity} from 'react-native';

const Pdfku = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#696969',
      }}>
      <Text
        style={{
          color: 'fff',
          marginBottom: 10,
        }}>
        My Pdf
      </Text>
      <TouchableOpacity></TouchableOpacity>
      <Button title="Open PDF Document" />

      <Pdf
        source={{
          uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf ',
        }} style={{height: 550, width: 550, marginTop: 10}}></Pdf>
        
    </View>
  );
};

export default Pdfku;
