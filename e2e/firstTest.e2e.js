describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });
  beforeEach(async () => {
    //await device.reloadReactNative();
  });

  it('should have Login screen', async () => {
    await expect(element(by.id('LoginView'))).toBeVisible();
  });

  it('should tap Register button', async () => {
    await element(by.id('RegisterButton')).tap();
  });

  it('should have Register screen', async () => {
    await expect(element(by.id('RegisterView'))).toBeVisible();
  });

  it('should fill Register form', async () => {
    await element(by.id('fullnameInput')).typeText('Gama');
    await element(by.id('emailInput')).typeText('Gama2022@gmail.com');
    await element(by.id('passwordInput')).typeText('Gama2020\n');
    await element(by.id('RegisterButton')).tap();
  });

  it('should have success register screen', async () => {
    await waitFor(element(by.id('SuccessView')))
      .toBeVisible()
      .withTimeout(5000);
    await expect(element(by.id('SuccessView'))).toBeVisible();
  });

  it('should tap successbutton', async () => {
    await element(by.id('successbutton')).tap();
  });

  // it('should tap Login button', async () => {
  //   await element(by.id('Login button')).tap();
  // });

  it('should fill login form', async () => {
    await element(by.id('emailInput')).typeText('Gama2022@gmail.com');
    await element(by.id('passwordInput')).typeText('Gama2020\n');
    await element(by.id('LoginButton')).tap();
  });

  it('should have Home screen', async () => {
    await expect(element(by.id('Homeview'))).toBeVisible();
  });

  it('should enable scroll horizontal', async () => {
    await element(by.id('scrollhorizontal')).swipe('left');
  });
  it('should enable scroll vertical', async () => {
    await element(by.id('scrollvertical')).scrollTo('bottom');
  });
});
