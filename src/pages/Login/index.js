import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {Buku} from '../../assets';
import { useDispatch } from 'react-redux';
import { login_action } from '../../redux/action';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const submit =() => {
    dispatch(login_action(email, password, navigation))
  }
  return (
    <View testID='LoginView'  style={styles.Login}>
      <View style={styles.image}>
        <Image source={Buku} style={styles.ImageLogin}></Image>
      </View>
      <View style={styles.container}>
        <TextInput
          value={email}
          onChangeText={(text) => setEmail(text)}
          style={styles.input}
          placeholder="Input  your email here"
          placeholderTextColor={'black'}
          testID="emailInput"
        />
        <TextInput
        value={password}
        onChangeText={(text) => setPassword(text)}
          style={styles.input}
          placeholder="Input  your password here"
          placeholderTextColor={'black'}
          testID="passwordInput"
        />
      </View>
      <View style={styles.LoginStyle}>
        <TouchableOpacity testID='LoginButton' onPress={() => submit()}>
          <View style={styles.LoginButton}>
            <Text>Login</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.account}>
        <Text>DON'T HAVE AN ACCOUNT</Text>
      </View>
      <View style={styles.Register}>
        <TouchableOpacity testID='RegisterButton' onPress={() => navigation.navigate('Register')}>
          <Text>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  input: {
    width: 250,
    height: 50,
    borderWidth: 2,
    borderRadius: 6,
    marginBottom: 6,
  },
  container: {
    alignItems: 'center',
    marginTop: 20,
  },
  ImageLogin: {
    width: 300,
    height: 200,
    marginTop: 30,
    borderRadius: 6,
  },
  image: {
    alignItems: 'center',
  },
  Login: {
    flex: 1,
    backgroundColor: '#A97155',
  },
  LoginButton: {
    backgroundColor: '#0093AB',
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6
  },
  LoginStyle: {
    alignItems: 'center',
    marginTop: 6,
  },
  account: {
    alignItems: 'center',
    marginTop: 6,
  },
  Register: {
    alignItems: 'center',
    marginTop: 6,
  },

});
