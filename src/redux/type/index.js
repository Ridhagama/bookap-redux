export const LOGIN_SUCCESS = '@LOGIN_SUCCESS'
export const LOGIN_FAILED = '@LOGIN_FAILED'
export const LOGIN_LOADING = '@LOGIN_LOADING'

export const LOGOUT ='@LOGOUT'

export const REGISTER_SUCCESS = '@REGISTER_SUCCESS'
export const REGISTER_FAILED = '@REGISTER_FAILED'
export const REGISTER_LOADING = '@REGISTER_LOADING'

export const HOME_SUCCESS = '@GET_SUCCESS'
export const HOME_FAILED = '@GET_FAILED'
export const HOME_LOADING = '@GET_LOADING'
export const HOMEPOPULAR = '@GET_POPULAR'

export const DETAIL_SUCCESS = "@GET_SUCCESS"
export const DETAIL_FAILED = "@GET_FAILED"
export const DETAIL_LOADING = "@GET_LOADING"

export const REFRESH_SUCCESS ="@REFRESH"

export const SET_ONLINE = '@SET_ONLINE'