import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, DetailBook, Login, Register, Splash, Success, Pdfku} from '../pages';
import Videoplay from '../pages/Videoplay';


const Stack = createNativeStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
         <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="Success"
        component={Success}
        options={{headerShown: false}}
      />
   
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />

     

      <Stack.Screen
        name="Detail Book"
        component={DetailBook}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Videoplay"
        component={Videoplay}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="Pdfku"
        component={Pdfku}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
